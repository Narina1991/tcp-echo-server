#include "tcpechoserver.h"
#include <cstring>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/sysinfo.h>

TcpEchoServer::TcpEchoServer(const uint16_t& port, const std::size_t& maxConnection,
                             const std::size_t& maxMessageLength, const std::size_t& secondOfDelay):
    m_maxConnection(maxConnection),
    m_maxMessageLength(maxMessageLength),
    m_buffer(maxMessageLength * maxConnection)
{
    initializeServerSocket(port);
    m_timout.tv_sec = secondOfDelay;

    io_uring_params params{};
    if(io_uring_queue_init_params(m_maxConnection, &m_ring, &params) < 0)
          throw (std::string("Error while init io_uring_queue params. ") + std::strerror(errno));
}

void TcpEchoServer::initializeServerSocket(const uint16_t& port)
{
    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    address.sin_addr.s_addr = INADDR_ANY;

    m_fileDescriptorServerSocket = socket(AF_INET, SOCK_STREAM, 0);

    const int val = 1;
    if(setsockopt(m_fileDescriptorServerSocket, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val)) == -1)
        throw std::string("Failed to set option to socket!");

    if (bind(m_fileDescriptorServerSocket, (struct sockaddr *)&address,
         sizeof(address)) < 0)
    {
        throw std::string("Failed to bind socket!");
    }

    if(listen(m_fileDescriptorServerSocket, SOMAXCONN))
        throw std::string("Can't prepare to listen socket");
}

io_uring_sqe* TcpEchoServer::getSQE()
{
    io_uring_sqe* sqe = io_uring_get_sqe(&m_ring);
    if(sqe == NULL)
        throw std::string("io_uring_get_sqe return NULL, the SQ ring is currently full");
    else
        return sqe;
}

void TcpEchoServer::accept()
{
    io_uring_sqe* sqe = getSQE();
    UserData data;
    data.fileDescriptor = m_fileDescriptorServerSocket;
    data.eventType = EventType::Accept;

    io_uring_prep_accept(sqe, m_fileDescriptorServerSocket,  nullptr, nullptr, 0);
    memcpy(&sqe->user_data, &data, sizeof(data));
}

void TcpEchoServer::recieve(const uint32_t& fileDescriptor)
{
    io_uring_sqe* sqe = getSQE();
    io_uring_prep_recv(sqe,fileDescriptor,  nullptr, m_maxMessageLength, 0);

    UserData userData;
    userData.eventType = EventType::Recieve;
    userData.fileDescriptor = fileDescriptor;
    memcpy(&sqe->user_data, &userData, sizeof(userData));

    io_uring_sqe_set_flags(sqe, IOSQE_BUFFER_SELECT);
}

void TcpEchoServer::provideBuffers(int id)
{
    io_uring_sqe* sqe = getSQE();

    UserData data;
    data.fileDescriptor = 0;
    data.eventType = EventType::ProvideBuffer;

    io_uring_prep_provide_buffers(sqe, m_buffer.data(), m_maxMessageLength, m_maxConnection, 0, id);
    memcpy(&sqe->user_data, &data, sizeof(data));
}

void TcpEchoServer::startEventLoop()
{
    provideBuffers(0);

    io_uring_submit(&m_ring);
    struct io_uring_cqe *cqe;
    io_uring_wait_cqe(&m_ring, &cqe);
    if(cqe->res < 0)
        throw (std::string("Can't provide buffer, result code: ") + std::to_string(cqe->res));
    io_uring_cqe_seen(&m_ring, cqe);

    accept();

    int cqeBufferSize = 100;

    while(1)
    {
        io_uring_submit_and_wait(&m_ring, 1);

        struct io_uring_cqe *cqe;
        struct io_uring_cqe *cqes[cqeBufferSize];

        int cqe_count = io_uring_peek_batch_cqe(&m_ring, cqes, cqeBufferSize);

        for (int i = 0; i < cqe_count; ++i) {
            cqe = cqes[i];
            UserData userData;
            memcpy(&userData, &cqe->user_data, sizeof(userData));

            if(userData.eventType == EventType::ProvideBuffer)
            {
                if(cqe->res < 0)
                    throw (std::string("Can't provide buffer, result code: ") + std::to_string(cqe->res));
            }
            if(userData.eventType == EventType::Accept)
            {
                recieve(cqe->res);
                accept();
            }
            else if(userData.eventType == EventType::Recieve)
            {
                if (cqe->res <= 0)
                {
                    provideBuffers(userData.bufID);
                    close(userData.fileDescriptor);
                }
                else
                {
                    io_uring_sqe* sqe = getSQE();

                    io_uring_prep_timeout(sqe, &m_timout, 0, 0);
                    userData.messageSize = cqe->res;
                    userData.eventType = EventType::Timout;
                    userData.bufID = cqe->flags >> 16;
                    memcpy(&sqe->user_data, &userData, sizeof(userData));

                    recieve(userData.fileDescriptor);
                }
            }
            else if(userData.eventType == EventType::Timout)
            {
                io_uring_sqe* sqe = getSQE();
                io_uring_prep_send(sqe, userData.fileDescriptor, m_buffer.data() + userData.bufID * m_maxMessageLength, userData.messageSize, 0);

                userData.eventType = EventType::Send;
                memcpy(&sqe->user_data, &userData, sizeof(userData));
            }
            else if(userData.eventType == EventType::Send)
                provideBuffers(userData.bufID);

            io_uring_submit(&m_ring);
        }
        io_uring_cq_advance(&m_ring, cqe_count);
    }
}
