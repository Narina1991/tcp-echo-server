#include "tcpechoserver.h"
#include <boost/program_options.hpp>

namespace po = boost::program_options;

int main(int argc, char* argv[])
{
    try
    {
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help, h", "produce help message")
            ("port, p", po::value<int>(), "set port number")
            ("lenght, l", po::value<int>()->default_value(1024), "maximum length of message")
            ("connections, c", po::value<int>()->default_value(4096), "maximum number of connections")
            ("timeout, t", po::value<int>()->default_value(5), "delay time of send message")
        ;

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << "\n";
            return 0;
        }

        if (vm.count("port") == 0)
        {
            std::cout << "Please give the port number in argument (see help).\n";
            return 0;
        }

        TcpEchoServer server(vm["port"].as<int>(), vm["connections"].as<int>(), vm["lenght"].as<int>(), vm["timeout"].as<int>());
        server.startEventLoop();
    }
    catch(std::exception& e) {
        std::cout << e.what() << "\n";
    }
    catch(const std::string& exc) {
        std::cout << exc << "\n";
    }

    return 0;
}
