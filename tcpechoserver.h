#ifndef TCPECHOSERVER_H
#define TCPECHOSERVER_H
#include <iostream>
#include <liburing.h>
#include <vector>

class TcpEchoServer
{
    enum EventType : uint16_t
    {
        ProvideBuffer,
        Accept,
        Recieve,
        Timout,
        Send,
    };

    struct UserData
    {
        uint16_t fileDescriptor;
        uint16_t bufID;
        EventType eventType;
        uint16_t messageSize;
    };

public:
    TcpEchoServer(const uint16_t& port, const std::size_t& maxConnection,
                  const std::size_t& maxMessageLength, const std::size_t& secondOfDelay);
    void startEventLoop();

private:
    void initializeServerSocket(const uint16_t& port);
    void accept();
    void recieve(const uint32_t& fileDescriptor);
    void provideBuffers(int id);
    io_uring_sqe* getSQE();

    int m_fileDescriptorServerSocket;
    io_uring m_ring;

    __kernel_timespec m_timout {};
    const std::size_t m_maxMessageLength;
    const std::size_t m_maxConnection;
    std::vector<char> m_buffer;
};

#endif // TCPECHOSERVER_H
